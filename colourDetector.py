#!/usr/bin/python3

import cv2
import numpy as np

cam = cv2.VideoCapture(0)

# cam.set(3, 20)
# cam.set(4, 40)

cv2.namedWindow("Colour Detector")

def meanColour(colourList):
    r = 0
    g = 0
    b = 0
    length = len(colourList)
    for i in colourList:
        r += int(i[0])
        g += int(i[1])
        b += int(i[2])
    r = r/length
    g = g/length
    b = b/length
    return ([r, g, b])

def binaryToDenary(num):
    denery = 0
    for index, element in enumerate(str(num)[::-1]):
        denery += int(element) * 2^int(index)
    return denery

def classifyColour(inputColour):
    colour = list(inputColour.copy())
    colourDict = {0: "black", 1: "blue", 2:"green", 3: "cyan", 4: "red", 5: "magenta", 6: "yellow", 7: "white"}

    if colour[0] > 128:
        colour[0] = 1
    else:
        colour[0] = 0

    if colour[1] > 128:
        colour[1] = 1
    else:
        colour[1] = 0

    if colour[2] > 128:
        colour[2] = 1
    else:
        colour[2] = 0

    colourNumber = 4*colour[2] + 2*colour[1] + colour[0]
    return colourDict[colourNumber]

def extremeColour(inputColour):
    colour = list(inputColour.copy())

    if colour[0] > 128:
        colour[0] = 255
    else:
        colour[0] = 0

    if colour[1] > 128:
        colour[1] = 255
    else:
        colour[1] = 0

    if colour[2] > 128:
        colour[2] = 255
    else:
        colour[2] = 0
    return colour

sizeOfSquare = 480

img = np.zeros((512, 512, 3), np.uint8)
centralImg = np.zeros((sizeOfSquare, sizeOfSquare, 3), np.uint8)
extremeColourImg = np.zeros((512, 512, 3), np.uint8)

if __name__ == '__main__':
    while True:
        ret, frame = cam.read()
        cv2.imshow("Colour Detector", frame)
        if not ret:
            break
        k = cv2.waitKey(1)

        if k%256 == 27:
            # ESC pressed
            print("Escape hit, closing...")
            break
        elif k%256 == 32:
            # SPACE pressed
            centre = (int(frame.shape[0]/2), int(frame.shape[1]/2))
            colourList = []

            '''
            for i in frame:
                for j in i:
                    colourList.append((j[2], j[1], j[0]))
                    # extremeColourImg[i][j] = extremeColour([j[2], j[1], j[0]])
            averageColour = meanColour(colourList)
            print(averageColour, classifyColour(averageColour))
            # cv2.imshow("extreme", extremeColourImg)
            # rect = cv2.rectangle(img, (0, 0), (255, 255), averageColour, 2)
            # cv2.imshow("rect", rect)
            '''
            iCount = 0
            for i in range(int(centre[0]-sizeOfSquare//2), int(centre[0]+sizeOfSquare//2)):
                jCount = 0
                for j in range(int(centre[1]-sizeOfSquare//2), int(centre[1]+sizeOfSquare//2)):
                    pixel = frame[i][j]
                    centralImg[iCount][jCount] = pixel
                    extremeColourImg[iCount][jCount] = extremeColour(pixel)
                    colourList.append(pixel)
                    jCount += 1
                iCount +=1

            averageColour = meanColour(colourList)
            print(averageColour, classifyColour(averageColour))
            cv2.imshow("extreme", extremeColourImg)
            # rect = cv2.rectangle(img, (0, 0), (255, 255), averageColour, 20)
            # cv2.imshow("rect", rect)
            cv2.imshow("central", centralImg)

            #'''

    cam.release()
    cv2.destroyAllWindows()
